# multiverse-services

Instalação
----
Ao clonar o repositório, execute o seguinte comando para efetuar o download das dependências:

    npm install

Rotas
----
A seguir se encontram todas as rotas da API, seus métodos e suas funções:

**Usuários**

    URL: /api/users
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os usuários cadastrados no sistema.

**Usuário Específico**

    URL: /api/users/:userId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Turma**

    URL: /api/classes
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os turmas cadastradas no sistema.

**Turma Específica**

    URL: /api/classes/:classId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Times**

    URL: /api/teams
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os times cadastrados no sistema.

**Time Específico**

    URL: /api/teams/:teamId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Módulos**

    URL: /api/modules
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os módulos cadastrados no sistema.

**Módulo Específico**

    URL: /api/modules/:moduleId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.     

**Competência Técnica**

    URL: /api/skills/technical
    MÉTODOS: GET
    FUNÇÃO: Retorna todas as competências técnicas cadastradas no sistema.

**Competênca Técnica Específica**

    URL: /api/skills/technical/:skillId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Competência Transversal**

    URL: /api/skills/transversal
    MÉTODOS: GET
    FUNÇÃO: Retorna todas as competências transversais cadastradas no sistema.

**Competênca Transversal Específica**

    URL: /api/skills/transversal/:skillId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Conteúdos**

    URL: /api/contents
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os conteúdos cadastrados no sistema.

**Módulo Específico**

    URL: /api/contents/:contendId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.           

**Encontros**

    URL: /api/meetings
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os encontros cadastrados no sistema.

**Módulo Específico**

    URL: /api/meetings/:meetingId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.

**Entregas**

    URL: /api/deliverables
    MÉTODOS: GET
    FUNÇÃO: Retorna todos as entregas cadastradas no sistema.

**Módulo Específico**

    URL: /api/deliverables/:deliverableId
    MÉTODOS: GET,POST,PUT,DELETE
    FUNÇÃO: Realizar a ação solicitada pela request.                                  

**Login:**

    URL: /oapi/login    
    MÉTODOS: POST
    FUNÇÃO: Validar o login de um usuário

*exemplo de objeto:*

    { 
        email: {type: String} ,
        password: {type: String} 
    }
    
    
**Signup:**

    URL: /oapi/signup
    MÉTODOS: POST
    FUNÇÃO: Cadastrar um novo usuário
    
*exemplo de objeto:*

    { 
        name: {type: String} ,
        password: {type: String} ,
        email: {type: String} ,
        birthDate: {type: Date},
        userType: {type: String},
        gender: {type: String}
    }
    
**Validate Token:**

    URL: /oapi/validateToken
    MÉTODOS: POST
    FUNÇÃO: Valida o token de um usuário
    
*exemplo de objeto:*

    { 
        token: {type: String}
    }

----
**As rotas a seguir são protegidas pelo token jwt:**

**Post About Me :**

    URL: /api/users/putAboutMe
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o Sobre Mim do aluno
    
*exemplo de objeto:*

    { 
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        aboutMe: {type: String}
    }

**Put Experiência:**

    URL: /api/users/putExperience
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o array de experiências do usuário. Para alterar, deve-se enviar o array de novo com
    o objeto alterado.
    
*exemplo de objeto:*
    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    { 
        experiences: [{
            description: {type: String},
            place: {type: String},
            beginDate: {type: Date},
            endDate: {type: Date, default: 'Atualmente'}
        }],
    }    

**Put Hobbies:**

    URL: /api/users/putHobbies
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o array de hobbies do usuário. Para alterar, deve-se enviar o array de novo com
    o objeto alterado.
    
*exemplo de objeto:*

    { 
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
        hobbies: [{type: String}]
    }

**Put Tagline:**

    URL: /api/users/putTagline
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o cargo do usuário
    
*exemplo de objeto:*

    { 
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
        tagline: {type: String}
    }   

**Put Achievements:**

    URL: /api/users/putAchievements
    MÉTODOS: PUT
    FUNÇÃO: Atualiza as conquistas do usuário. Para alterar, deve-se enviar o array de novo com
    o objeto alterado.
    
*exemplo de objeto:*

    { 
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
        achievements:{
            name: {type: String},
            yearAndMonth: {type: Date},
            description: {type: String}
        }
    }

**Put Competência em Destaque:**

    URL: /api/users/putHighlightedSkills
    MÉTODOS: PUT
    FUNÇÃO: Atualiza as competências em destaque de um usuário. Para alterar, deve-se enviar o array de novo com o objeto alterado.
    
*exemplo de objeto:*

    { 
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
        skills: [{type: mongoose.Schema.Types.ObjectId, ref: 'Transversal'} ou {type: mongoose.Schema.Types.ObjectId, ref: 'Technical'}]
    }         


**Get Hobbies por turma:**

    URL: /api/classes/:classId/getGenders
    MÉTODOS: GET
    FUNÇÃO: Retorna a quantidade por genêro dos alunos por turma
    
*exemplo de parâmetro:*

    { 
       classId: {type: mongoose.Schema.Types.ObjectId, ref: 'Class'} 
    }    

**Get time por usuário**

    URL: /api/modules/:moduleId/:userId/findTeam
    MÉTODOS: GET
    FUNÇÃO: Retorna o time de um aluno em tal módulo
    
*exemplo de parâmetro:*

    { 
       moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}, 
       userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'} 
    }

**GET Skills de um usuário**

    URL: /api/users/:userId/getSkills
    MÉTODOS: GET
    FUNÇÃO: Retorna as competências de um usuário
    
*exemplo de parâmetro:*

    { 
       userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'} 
    }             

**Criação de Módulo:**

**A atualização de um módulo é feita de maneira parcial através de várias rotas*

    URL: /api/modules/createModule
    MÉTODOS: POST
    FUNÇÃO: Cadastra um novo módulo no sistema
    
*exemplo de objeto:*

    { 
        title: {type: String},
        description: {type: String}
        classes: [{type: mongoose.Schema.Types.ObjectId, ref: 'Class'}],
        relatedTeachers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}] 
    }

**Questionários/Avaliações do usuário de um  módulo:**

    URL: /api/modules/:moduleId/:userId/getQuestionnaries
    MÉTODOS: GET
    FUNÇÃO: Retorna 3 booleanos a respeito dos questionários que o usuário pode fazer
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    }    

**Atualização de módulo (Habilidades Técnicas) :**

    URL: /api/modules/putTechnicalSkill
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de habilidades técnicas no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        newTechnicalSkills: [
            {
                _id: {type: mongoose.Schema.Types.ObjectId} // Se estiver vazio, uma nova habilidade técnica será criada
                name: {type: String},
                indicators: [{type: String}],
                ownerTeacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
                module: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}
            }
        ]
    }
    
**Atualização de módulo (Habilidades Transversais) :**

    URL: /api/modules/putTransversalSkill
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de habilidades transversáis no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        newTransversalSkills: [
            {
                _id: {type: mongoose.Schema.Types.ObjectId} // Se estiver vazio, uma nova habilidade técnica será criada
                name: {type: String},
                indicators: [{type: String}],
                ownerTeacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
                module: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}
            }
        ]
    }
    
**Atualização de módulo (Questão Foco) :**

    URL: /api/modules/putFocusQuestion
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de questão foco no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newFocusQuestions: [{type: String}]
    }
    
**Atualização de módulo (Situação Problema) :**

    URL: /api/modules/putProblemSituation
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de situação problema no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newProblemSituations: [
            {
                description: {type: String},
                files: [{type: String}]
            }
        ]
        
        
    }
    
**Atualização de módulo (Entregas) :**

    URL: /api/modules/putDeliveries
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de entregas no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newDeliverables: [
            {
                _id: {type: mongoose.Schema.Types.ObjectId} // Se estiver vazio, uma nova habilidade técnica será criada
                module: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
                title: {type: String},
                ownerTeacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
                description: {type: String},
                deliverDate: {type: Date},
                deliverType: {type: String, enum:['Artigo Ciêntifico', 'Trabalho Oral']},
                evaluationCriteria: [{
                    criteriaDescription: {type: String},
                    unsatisfactory: {type: String},
                    satisfactory: {type: String},
                    exemplary: {type: String},
                }]
            }
        ]
    }
    
**Atualização de módulo (Conteúdo) :**

    URL: /api/modules/putContent
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de conteúdo no modulo indicado
    
*exemplo de objeto:*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newContents: [
            {
                _id: {type: mongoose.Schema.Types.ObjectId} // Se estiver vazio, uma nova habilidade técnica será criada
                description: {type: String},
                archives: [{type: String}]
                module: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}
            }
        ]
    }
    
**Atualização de módulo (Cronograma de Encontros) :**

    URL: /api/modules/putMeetings
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de encontros no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newMeetings: [
            _id: {type: mongoose.Schema.Types.ObjectId} // Se estiver vazio, uma nova habilidade técnica será criada
            title: {type: String, requried: true},
            contents: [{type: mongoose.Schema.Types.ObjectId, ref: 'Content'}],
            methodology: {type: String},
            meetingDate: {type: Date},
            module: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
            skills: {
                technicalSkills: [{type: mongoose.Schema.Types.ObjectId, ref: 'Technical'}],
                transversalSkills: [{type: mongoose.Schema.Types.ObjectId, ref: 'Transversal'}]
            }
        ]
    }
    
**Atualização de módulo (Avaliações) :**

    URL: /api/modules/putEvaluation
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de avaliações no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newEvaluation: {
            form: {
                technicalSkills: [{
                    skill: {type: mongoose.Schema.Types.ObjectId, ref: 'Technical'},
                    teacher: {type: Boolean, default: false},
                    team: {type: Boolean, default: false},
                    student: {type: Boolean, default: false},
                }],
                transversalSkills: [{
                    skill: {type: mongoose.Schema.Types.ObjectId, ref: 'Technical'},
                    teacher: {type: Boolean, default: false},
                    team: {type: Boolean, default: false},
                    student: {type: Boolean, default: false},
                }]
            },
            sum: [{
                deliverables:{type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
                teacher: {type: Boolean, default: false},
                team: {type: Boolean, default: false},
                student: {type: Boolean, default: false},
                },
            }]
        }
    }
    
**Atualização de módulo (Recursos) :**

    URL: /api/modules/putResources
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de recursos no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},,
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newResources: [{type: String}]
    }

**Atualização de módulo (Descrição) :**

    URL: /api/modules/updateDescription
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o campo de descrição no modulo indicado
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        description: {type: String},
    }

**Atualização de módulo (Questionário de Começo) :**

    URL: /api/modules/putStartQuestionnarie
    MÉTODOS: PUT
    FUNÇÃO: Adiciona um questionário de começo do módulo
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        questionnarie:{
            studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            answers: [{type: Number, min:0, max: 10}] 
        }
    }

**Atualização de módulo (Questionário de Fim) :**

    URL: /api/modules/putEndQuestionnarie
    MÉTODOS: PUT
    FUNÇÃO: Adiciona um questionário de fim do módulo
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        questionnarie:{
            studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            answers: [{type: String, enum: ['Exemplar','Satisfatório', 'Insastifatório']}],
            generalComments: {type: String} 
        }
    }

 **Atualização de módulo (Questionário de Fim) :**

    URL: /api/modules/updateModuleStatus
    MÉTODOS: PUT
    FUNÇÃO: Altera o atributo que juga se o módulo está encerrado ou não
    
*exemplo de objeto:*

    { 
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        status: {type: Boolean}
    }         
            

**Avaliação de Conteúdos** 

    URL: /api/modules/putContentEvaluation
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação do conteúdo ao usuário associado

*exemplo de objeto*

    {
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        grade: {type: Number},
        contentId: {type: mongoose.Schema.Types.ObjectId, ref: 'Content'}
    }

**Avaliação de Entregas** 

    URL: /api/modules/putDeliverableEvaluation
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação da entrega de um usuário associado

*exemplo de objeto*

    {
        deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newSelfEvaluation: {
            rating: {type: String},
            ratingJustification: {type: String}
        },
        newCriterias: [{
            criteiraId: {type: String},
            criteriaDescription: {type: String},
            rating: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        }]
    }

**Parecer Final de Entregas** 

    URL: /api/modules/putDeliverableFinal
    MÉTODOS: PUT
    FUNÇÃO: Faz um parecer final para a entrega associada.

*exemplo de objeto*

    {
        deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
        evaluator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        evaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        comment: {type: String}
    }

**Adicionar Acompanhamento de Entregas** 

    URL: /api/deliverable/postFollowUp
    MÉTODOS: POST
    FUNÇÃO: Adiciona um novo comentário de acompanhamento

*exemplo de objeto*

    {
        deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
        teacherId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        comment: {type: String}
    }

**Retornar Acompanhamento de Entregas** 

    URL: /api/deliverables/:deliverableId/:studentId/getFollowUp
    MÉTODOS: GET
    FUNÇÃO: Retorna os acompanhamentos de um professor para certo aluno

*exemplo de objeto*

    {
        deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    }   

**Parecer Final de Competência** 

    URL: /api/skills/:type/:skillId/:userId/getFinalOpinion
    MÉTODOS: GET
    FUNÇÃO: Retorna o parecer final de uma competência

*exemplo de parâmetro*

    {
        skillId: {type: mongoose.Schema.Types.ObjectId},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        type: {type: String, enum: ['Technical','Transversal']}
    }

**Avaliação de Competências Técnicas** 

    URL: /api/modules/putTechnicalRating
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação de uma competência técnica de um usuário associado.
    OBS: newTeamMatesEvaluation é opcional, relacionada com quem está avaliando o que

*exemplo de objeto*

    {
        technicalSkillId: {type: mongoose.Schema.Types.ObjectId, ref: 'Technical'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newSelfEvaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        newTeamMatesEvaluation: [{
            studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            rating: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
            justification: {type: String}
        }]
    }

**Parecer Final de Competências Técnicas** 

    URL: /api/modules/putTechnicalFinal
    MÉTODOS: PUT
    FUNÇÃO: Faz um parecer final para a competência técnica associada.

*exemplo de objeto*

    {
        technicalSkillId: {type: mongoose.Schema.Types.ObjectId, ref: 'Technical'},
        evaluator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        evaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        comment: {type: String}
    }    

 **Avaliação de Competências Transversais** 

    URL: /api/modules/putTransversalRating
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação de uma competência transversal de um usuário associado
    OBS: newTeamMatesEvaluation é opcional, relacionada com quem está avaliando o que

*exemplo de objeto*

    {
        transversalSkillId: {type: mongoose.Schema.Types.ObjectId, ref: 'Transversal'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newSelfEvaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        newTeamMatesEvaluation: [{
            studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            rating: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']}
        }]
    }

**Parecer Final de Competências Transversais** 

    URL: /api/modules/putTransversalFinal
    MÉTODOS: PUT
    FUNÇÃO: Faz um parecer final para a competência transversal associada.

*exemplo de objeto*

    {
        transversalSkillId: {type: mongoose.Schema.Types.ObjectId, ref: 'Transversal'},
        evaluator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        evaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']},
        comment: {type: String}
    }
    
**Avaliação de Entregas pelo Professor**
    
    URL: /api/modules/putDeliverableTeacherEvaluation
    MÉTODOS: PUT
    FUNÇÃO: Faz a avaliação do entregavel pelo professor.
    
*exemplo de objeto*

    {
        evaluatorId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        criteriasEvaluation: [{
            criteriaId: {type: String}
            criteriaDescription: {type: String},
            rating: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']}
        }]
        
    }

  **Avaliação de Participação de Encontros** 

    URL: /api/modules/putMeetingParticipation
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação de participação de um encontro associado a um usuário

*exemplo de objeto*

    {
        meetingId: {type: mongoose.Schema.Types.ObjectId, ref: 'Meeting'}, 
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newSelfEvaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']}
    }         

**Avaliação de Aproveitamento Encontros** 

    URL: /api/modules/putMeetingEnjoyable
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação de aproveitamento de um encontro associado a um usuário

*exemplo de objeto*

    {
        meetingId: {type: mongoose.Schema.Types.ObjectId, ref: 'Meeting'}, 
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        newEnjoyable: {type: String}
    }

**Avaliação do Professor sobre Participação** 

    URL: /api/modules/putMeetingTeacherEvaluation
    MÉTODOS: PUT
    FUNÇÃO: Gera uma avaliação do professor para a participação dos alunos

*exemplo de objeto*

    {   
        meetingId: {type: mongoose.Schema.Types.ObjectId, ref: 'Meeting'},
            [teacherId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            students: [{
                studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
                evaluation: {type: String, enum:['Exemplar', 'Satisfatório', 'Insatisfatório']}
            }]
        ]
    }  
    
**Listar todos os cards de um Kanban** 

    URL: /api/modules/kanban/:moduleId/:userId/getCards
    MÉTODOS: GET
    FUNÇÃO: Listar todos cards de um kanban (equipe especifica em um modulo)
    
*parametros de rota*

    moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'module'} (*em string*)
    teamId: {type: mongoose.Schema.Types.ObjectId, ref: 'team'} (*em string*)
    
**Adicionar um novo card ao Kanban** 

    URL: /api/modules/kanban/postNewCard
    MÉTODOS: POST
    FUNÇÃO: Adicionar um novo card a um kanban especifico
    
*exemplo de objeto*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'module'},
        teamId: {type: mongoose.Schema.Types.ObjectId, ref: 'team'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        state: {type: String, enum: ['A Fazer','Fazendo','Feito']},
        content: {type: String}
    }

**Alterar um card no Kanban** 

    URL: /api/modules/kanban/updateCard
    MÉTODOS: PUT
    FUNÇÃO: fazer alterações no estado e conteúdo de um card
    
*exemplo de objeto*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'module'},
        teamId: {type: mongoose.Schema.Types.ObjectId, ref: 'team'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        cardId: {type: mongoose.Schema.Types.ObjectId, ref: 'card'},
        state: {type: String, enum: ['A Fazer','Fazendo','Feito']},
        content: {type: String}
    }

**Deletar um card no Kanban** 

    URL: /api/modules/kanban/deleteCard
    MÉTODOS: PUT
    FUNÇÃO: deletar um card no kanban
    
*exemplo de objeto*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        teamId: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        cardId: {type: mongoose.Schema.Types.ObjectId, ref: 'Card'}
    }

**Upload de Arquivos** 

    URL: /api/deliverable/uploadFile
    MÉTODOS: POST
    FUNÇÃO: Faz o upload de um arquivo
    
*exemplo de objeto*
    
    {
        body: {
            deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'},
            studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
        },
        files: {
            file: {type: file}
        }
    }
    
**Upload de Arquivos** 

    URL: /api/deliverable/downloadFile/:deliverableId/:studentId
    MÉTODOS: GET
    FUNÇÃO: Faz o download de um arquivo
    
*exemplo de parametros*
    
    studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    deliverableId: {type: mongoose.Schema.Types.ObjectId, ref: 'Deliverable'}
    
**Adicionar/Atualizar Time em um Módulo :**

    URL: /api/modules/team/putTeam
    MÉTODOS: PUT
    FUNÇÃO: Atualiza ou insere um time no módulo espeçificado
    
*exemplo de objeto:*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        newTeams: [{
            teamName: {type: String, required: true},
            members: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
            teamLeader: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}            
        }]
    }
    
**Lista de todos os Entregáveis e Encontros de um usuário**

    URL: /api/users/getDeliverablesAndMeetings
    MÉTODOS: GET
    FUNÇÃO: Lista todos os entregáveis e encontros de um usuário divididos por modulo.
    
*exemplo de objeto:*

    {
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    }

**Finalizar conteúdo autodirigido**

    URL: /api/modules/putFinalizedContent
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o estado de conteudo autodirigido de uma aluno
    
*exemplo de objeto:*

    {
        contentId: {type: mongoose.Schema.Types.ObjectId, ref: 'Content'},
        studentId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        classId: {type: mongoose.Schema.Types.ObjectId, ref: 'Class'}
        hasFinished: {type: Boolean}
    }

**Adicionar Duvida :**

    URL: /api/modules/putDoubts
    MÉTODOS: PUT
    FUNÇÃO: Insere uma duvida no conteúdo
    
*exemplo de objeto:*

    {
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        contentId: {type: mongoose.Schema.Type.ObjectId, ref: 'Content'},
        question: {type: String}
    }

**Gerar times aleatórios :**

    URL: /api/modules/team/newRandomTeams
    MÉTODOS: PUT
    FUNÇÃO: Gera times aleatórios para certo módulo
    
*exemplo de objeto:*

    {
        usersArray: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
        teamSize: {type: Number},
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'}
    }

**Deletar um time :**

    URL: /api/modules/team/deleteTeam
    MÉTODOS: PUT
    FUNÇÃO: Deleta um time
    
*exemplo de objeto:*

    {
        moduleId: {type: mongoose.Schema.Types.ObjectId, ref: 'Module'},
        teamId: type: mongoose.Schema.Types.ObjectId, ref: 'Team'
    }                  

**Lista de Professores :**

    URL: /api/users/getTeachers
    MÉTODOS: GET
    FUNÇÃO: Listar todos professores cadastrados

**Lista de Modulos do Usuário :**

    URL: /api/users/:userId/getModuleByUser
    MÉTODOS: GET
    FUNÇÃO: Listar todos modulos do usuáro (Professor e Aluno)

**Exibe os dados do Currículo do Usuário :**

    URL: /api/users/:userId/getCurriculum
    MÉTODOS: GET
    FUNÇÃO: Listar os dados a respeito do currículo de um usuário

    *exemplo de parâmetro*

    {
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    }

**Lista de Conteúdos do Modulo :**

    URL: /api/modules/:moduleId/getContent
    MÉTODOS: GET
    FUNÇÃO: Listar todos os conteúdos do modulo especifico

**Lista de encontros do Modulo :**

    URL: /api/modules/:moduleId/getMeetings
    MÉTODOS: GET
    FUNÇÃO: Listar todos os encontros do modulo especifico

**Lista de entregas do Modulo :**

    URL: /api/modules/:moduleId/getDeliveries
    MÉTODOS: GET
    FUNÇÃO: Listar todos as entregas do modulo especifico

**Lista de habilidades/competências do Modulo :**

    URL: /api/modules/:moduleId/getSkills
    MÉTODOS: GET
    FUNÇÃO: Listar todos as habilidades/competências do modulo especifico

**Turmas por Módulo:**

    URL: /api/modules/:moduleId/getClassByModule    
    MÉTODOS: GET
    FUNÇÃO: Retorna as classes de um determinado módulo    
    